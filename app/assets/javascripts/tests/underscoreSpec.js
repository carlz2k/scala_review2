define([
    "module",
    "exercises/underscore"
], function(
    self,
    underscore
) {
    return function() {
        module(self.id);
    };
});