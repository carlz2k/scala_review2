define([
    "module",
    "exercises/example"
], function(
    self,
    example
) {
    return function() {
        module(self.id);

        test("Example test", function() {
            equal(example.getExampleString(), "Example", "Example returned 'Example'");

            notEqual(example.getExampleString(), "Not example", "Example did not return 'Not example'");
        });
    };
});