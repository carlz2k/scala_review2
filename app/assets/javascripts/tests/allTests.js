/**
 * @preserve Copyright (c) [2010-2015] Visier Solutions Inc. All rights reserved.
 */

define([
    "tests/exampleSpec",
    "tests/generalQuestionsSpec",
    "tests/functionsSpec",
    "tests/objectsSpec",
    "tests/thisSpec",
    "tests/underscoreSpec"
], function (
    exampleSpec,
    generalQuestionsSpec,
    functionsSpec,
    objectsSpec,
    thisSpec,
    underscoreSpec
) {
    /**
     * Runs all planning related unit tests.
     */
    return function () {
        exampleSpec();
        generalQuestionsSpec();
        functionsSpec();
        objectsSpec();
        thisSpec();
        underscoreSpec();
    };
});


