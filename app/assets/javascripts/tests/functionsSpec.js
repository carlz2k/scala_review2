define([
    "module",
    "exercises/functions"
], function(
    self,
    functions
) {
    return function() {
        module(self.id);
    };
});