package com.visiercorp.src.main.java;

//Q9
class Car {
    private String manufacturer;
    private String name;
    private int modelYear;
    public String licensePlate; //for simplicity purpose so that we don't need to create getter setter

    //primary constructor that can be called by other constructors with hidden default values
    public Car(String manufacturer, String name, int modelYear, String licensePlate) {

        this.manufacturer = manufacturer;
        this.name = name;
        this.modelYear = modelYear;
        this.licensePlate = licensePlate;
    }

    public Car(String manufacturer, String name) {
        this(manufacturer, name, -1, "");
    }


    public Car(String manufacturer, String name, int modelYear) {
        this(manufacturer, name, modelYear, "");
    }


    public Car(String manufacturer, String name, String licensePlate) {
        this(manufacturer, name, -1, licensePlate);
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getName() {
        return name;
    }

    public int getModelYear() {
        return modelYear;
    }
}