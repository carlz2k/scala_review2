package com.visiercorp.src.main.scala

//Q
class Complex(val real: Int, val imaginary: Int) {
  // TODO This method should probably be in the Complex class
  //i thought the question was asking to create a method with two params.
  def add(complex2: Complex): Complex = {
    new Complex(this.real + complex2.real, this.imaginary + complex2.imaginary)
  }
}

object ImplicitConversion {
  implicit def convert(value: Int): Complex = new Complex(value, 0)

  def multiply(x: Int)(implicit by: Int): Int = x * by
}