package com.visiercorp.src.main.scala

object ComplementaryPairs {
  def solution(K: Int, A: Vector[Int]): Int = {

    //TODO Shouldn't need any mutable collections or vars for this question - have you encountered the groupBy method?

    val tempMap = A.groupBy(x => x)

    A.foldLeft(0)(
      (acc, x) => {
        acc + tempMap.getOrElse(K - x, Map()).size
      }
    )
  }
}

