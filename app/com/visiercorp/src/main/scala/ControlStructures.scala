package com.visiercorp.src.main.scala

import scala.annotation.tailrec

object ControlStructures {
  //Q6
  def toUnicodeWithLoop(input: String): Long = {
    var result: Long = 1
    for (char <- input) result *= char.toLong
    result
  }

  //Q7
  def toUnicodeWithoutLoop(input: String): Long = {
    input.foldLeft(1L)(_ * _)
  }

  //Q8
  def toUnicodeWithoutLoopFunctionImplementation(input: String): Long = {
    //have to write another wrapper function just to meet the requirement of Q8
    toUnicodeWithoutLoop(input)
  }

  //Q9
  def toUnicodeTailRecursive(input: String): Long = {
    @tailrec
    //any suggestion how to make it tail recursive?
    def toUnicodeHelper(i: String, acc: Long): Long = {
      i match {
        case "" => acc
        case _ => toUnicodeHelper(i.tail, i.head * acc)
      }
    }

    toUnicodeHelper(input, 1)
  }

  //Q10
  def power(x: Int, n: Int): Double = {
    n match {
      case 0 => 1
      case _ if n < 0 => 1 / power(x, n * (-1))
      case _ if n % 2 == 0 =>
        val temp = power(x, n / 2)
        temp * temp
      case _ => x * power(x, n - 1)
        //how was it covered by the previous case? I am curious
        // TODO because if n % 2 == 0, we go into the second last case. therefore, if we get to this case, then by definition
        // n % 2 must not be equal to 0 (kind of like how you don't explicitly check that n != 0 and n > 0 after you've
        // passed the cases for them.
        // now i got it, i thought you wanted me to remove the entire check, not just the guard
    }
  }
}