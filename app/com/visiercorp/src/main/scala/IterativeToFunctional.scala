package com.visiercorp.src.main.scala

object IterativeToFunctional {
  //Q1
  def foo1(numbers: Array[Int]): Int = {
    numbers match {
      case Array() => 0
      case _ => numbers.reduce(_ + _) //seems like there is a "sum" method for array
    }
    //TODO Yeah, there is, and it'll work too, but the question wants a higher order function. Though this won't cover the edge case of an empty array.
  }

  //Q2
  def reverse(s: String): String = {
    //    val buf = new StringBuilder
    //    val len = s.length
    //    for (i <- 0 until len) {
    //      buf.append(s.charAt(len - i - 1))
    //    }
    //    buf.toString

    s.foldLeft("")((a, b) => {b + a})
  }

  //Q3
  def foo2(str: String): Boolean = {
    //    val arr = Array[String]("scala", "akka")
    //    var i = 0
    //    while ( {i < arr.length}) {
    //      if (str.contains(arr(i))) return true
    //    }
    //    return false
    val arr = Array[String]("scala", "akka")
    arr.exists(str.contains(_))
  }

  //Q4
  def foo3(strings: Array[String]): Array[String] = {
    //    val newString = new Array[String](strings.length * 2)
    //    var i = 0
    //    while ( {i < strings.length}) {
    //      newString(2 * i) = strings(i)
    //      newString(2 * i + 1) = strings(i)
    //    }
    //TODO instead of zipping with itself, you can create Array(x, x) in the flatMap
    strings.flatMap(x => Array(x, x))
  }

  //Q5
  def foo4(names: Seq[String], numbers: Seq[Int]): Seq[(String, Int)] = {
    //    // names and numbers are known to have the same length
    //    import scala.collection.mutable.ListBuffer
    //    val list = new ListBuffer[(String, Int)]
    //    for (i <- 0 until names.length) {
    //      list += ((names(i), numbers(i)))
    //    }
    //    list
    names.zip(numbers)
  }
}