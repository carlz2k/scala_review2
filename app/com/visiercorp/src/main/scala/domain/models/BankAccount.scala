package com.visiercorp.src.main.scala.domain.models

/**
  * The domain BankAccount model.
  * @param firstName The bank account owner's first name.
  * @param lastName The bank account owner's last name.
  * @param balance The bank account balance.
  */
case class BankAccount(firstName: String, lastName: String, balance: BigDecimal)
