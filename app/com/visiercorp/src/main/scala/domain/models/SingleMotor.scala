package com.visiercorp.src.main.scala.domain.models

trait SingleMotor {
  this: Chargeable =>

  val numberOfMotors = 1
}
