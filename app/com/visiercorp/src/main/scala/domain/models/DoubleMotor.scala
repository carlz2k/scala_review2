package com.visiercorp.src.main.scala.domain.models

//for simplicity, but should create a parent trait/class called Motor
trait DoubleMotor extends SingleMotor {

  this: LargeBattery =>

  override val numberOfMotors = 2
}
