package com.visiercorp.src.main.scala

object JavaInterop {
  //Q7
  //instread playing with generics, assume it is an array of integers
  //TODO you can stick with a generic array, you'll need to declare a type variable like def distinct[T]
  def distinct[T](array: Array[T]): Array[T] = {
    array.distinct
  }

  val AmericanTimeZonePrefix = "America/"

  //Q9
  def getAmericanTimezones: Array[String] = {
    java.util.TimeZone.getAvailableIDs.filter(
      //TODO This filter will give a false positive if the text America/ appears anywhere else in the string.
      // You may want to consider startsWith, and avoiding the toLowerCase calls
      _.toLowerCase().startsWith(AmericanTimeZonePrefix.toLowerCase())
    ).map(
      //TODO This will remove all instances of America/, instead of just the prefix
      _.replaceFirst(AmericanTimeZonePrefix, "")
    ).sorted
  }
}