package com.visiercorp.src.main {

  //Q3
  package object random {

    import org.joda.time.DateTime

    private var previous: Int = new DateTime().getMillis.toInt
    val a = 1664525
    val b = 1013904223
    val n = 32

    def nextDouble: Double = {
      //TODO will this give you a good range of doubles? I think this will only give you whole numbers.
      nextInt.toDouble
    }

    def nextInt: Int = {
      previous = (previous * a + b) % math.pow(2.0, n).toInt
      previous
    }

    def setSeed(seed: Int): Unit = {
      previous = seed
    }
  }

}