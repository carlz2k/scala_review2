package controllers

import play.api.mvc.{AnyContent, Action, Controller}

class TestController extends Controller {
  def index(): Action[AnyContent] = Action {
    Ok(views.html.tests())
  }
}
