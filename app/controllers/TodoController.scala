package controllers

import play.api.mvc.{AnyContent, Action, Controller}

class TodoController extends Controller{
  def index(): Action[AnyContent] = Action{
    Ok(views.html.todo())
  }
}
