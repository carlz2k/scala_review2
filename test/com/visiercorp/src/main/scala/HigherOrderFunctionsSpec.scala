package com.visiercorp.src.main.scala

import org.scalatest.{Matchers, FunSpec}

class HigherOrderFunctionsSpec extends FunSpec with Matchers {
  describe("find max in a sequence") {

    it("empty check") {
      HigherOrderFunctions.largest(
        x => {
          x * 5 + 2
        }, List()
      ) shouldEqual Int.MinValue
    }

    it("normal run") {
      HigherOrderFunctions.largest(
        x => {
          x * 5 + 2
        }, List(3, 5, 2, 1, 8, 4)
      ) shouldEqual 42
    }

    it("normal run sample in the book") {
      HigherOrderFunctions.largest(
        x => 10 * x - x * x, 1 to 10
      ) shouldEqual 25
    }
  }

  describe("find the position of max in a sequence") {

    it("normal run") {
      HigherOrderFunctions.largestAt(
        x => {
          x * 5 + 2
        }, List(3, 5, 2, 1, 8, 4)
      ) shouldEqual 8
    }

    it("normal run sample in the book") {
      HigherOrderFunctions.largestAt(
        x => 10 * x - x * x, 1 to 10
      ) shouldEqual 5
    }
  }

  describe("adjusts to pair for map") {
    it("normal run") {
      HigherOrderFunctions.adjustToPair(_ * _)((6, 7)) shouldEqual 42
    }
  }
}
