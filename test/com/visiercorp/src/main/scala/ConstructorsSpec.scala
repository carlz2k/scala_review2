package com.visiercorp.src.main.scala

import org.scalatest.{Matchers, FunSpec}

class ConstructorsSpec extends FunSpec with Matchers {
  val licensePlate = "lic"
  val manufacturer = "man"
  val name = "name"
  val modelYear = 333
  val newLicensePlate = "new lic"

  describe("car with 3 auxiliary constructors") {
    it("4 forms of constructors should work") {
      val car1: Car = new Car("ab", "cd", 5, "dsf")
      val car2: Car = new Car("ab", "cd", 5)
      val car3: Car = new Car("ab", "cd", "dsf")
      val car4: Car = new Car("ab", "cd")
    }

    it("only licensePlate support read and write") {
      val car1: Car = new Car(manufacturer, name, modelYear, licensePlate)
      car1.licensePlate shouldEqual licensePlate
      car1.licensePlate = newLicensePlate
      car1.getManufacturer shouldEqual manufacturer
      car1.getName shouldEqual name
      car1.getModelYear shouldEqual modelYear
      car1.licensePlate shouldEqual newLicensePlate
    }
  }

  describe("car with 1 auxiliary constructor") {
    it("4 forms of constructors should work") {
      val car1: CarAlt = new CarAlt("ab", "cd", -1, "dsf")
      val car2: CarAlt = new CarAlt("ab", "cd", 5)
      val car3: CarAlt = new CarAlt("ab", "cd", licensePlate = "dsf")
      car3.licensePlate shouldEqual "dsf"
      car3.modelYear shouldEqual -1
      val car4: CarAlt = new CarAlt("ab", "cd")
    }

    it("only licensePlate is read/write") {
      val car: CarAlt = new CarAlt(manufacturer, name, modelYear, licensePlate)
      car.licensePlate shouldEqual licensePlate
      car.licensePlate = newLicensePlate

      car.manufacturer shouldEqual manufacturer
      car.name shouldEqual name
      car.modelYear shouldEqual modelYear
      car.licensePlate shouldEqual newLicensePlate
    }
  }

  describe("rewrite Employee Class with explicit fields") {
    it("2 forms of constructors should work") {
      val employee = new Employee()
      val employee2 = new Employee("my name", 5000)
    }

    it("only salary is read/write") {
      val name = "my name"
      val salary = 5000
      val newSalary = 6000
      val employee = new Employee(name, salary)

      employee.getName shouldEqual name
      employee.salary shouldEqual salary
      employee.salary = newSalary
      employee.salary shouldEqual newSalary
    }
  }
}
