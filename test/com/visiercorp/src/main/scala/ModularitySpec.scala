package com.visiercorp.src.main.scala

import org.scalatest.{FunSpec, Matchers}
import com.visiercorp.src.main.random

class ModularitySpec extends FunSpec with Matchers {
  describe("random generator package") {
    it("no int overflow") {
      random.setSeed(Int.MaxValue)
      val v = random.nextInt
      v should not be 0
    }

    it("random next double without seed") {
      val v1 = random.nextDouble
      val v2 = random.nextDouble
      v1 should not be 0
      v2 should not be 0
      v1 should not equal v2
    }

    it("random next int without seed") {
      val v1 = random.nextInt
      val v2 = random.nextInt
      val v3 = random.nextInt
      v1 should not be 0
      v2 should not be 0
      v1 should not equal v2
    }

    it("random next int with seed") {
      val seed = 2344
      random.setSeed(seed)

      val v1 = random.nextInt
      v1 shouldEqual (seed * random.a + random.b) % math.pow(2.0, random.n).toInt

      val v2 = random.nextInt

      v2 shouldEqual (v1 * random.a + random.b) % math.pow(2.0, random.n).toInt

      v1 should not equal v2
    }

    it("random next int and next double mixture") {
      val v1 = random.nextInt
      val v2 = random.nextDouble
      random.setSeed(Int.MaxValue)
      v1 should not be 0
      v2 should not be 0
      v1 should not equal v2
    }
  }
}
