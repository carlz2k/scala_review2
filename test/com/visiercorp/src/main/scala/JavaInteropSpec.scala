package com.visiercorp.src.main.scala

import com.visiercorp.src.main.scala.JavaInterop.AmericanTimeZonePrefix
import org.scalatest.{FunSpec, Matchers}

class JavaInteropSpec extends FunSpec with Matchers {
  describe("remove duplicates") {
    it("multiple elements test") {
      JavaInterop.distinct(Array(2, 4, 9, 2, 3, 4, 7, 5)) shouldEqual Array(2, 4, 9, 3, 7, 5)
    }
  }

  describe("process java timezone for American timezones") {
    it("execute") {
      val result = JavaInterop.getAmericanTimezones

      for (value <- result) {
        if (value.toLowerCase().contains(AmericanTimeZonePrefix.toLowerCase())) {
          fail("should not contain '" + AmericanTimeZonePrefix + "'")
        }
      }

      result.toList.sorted shouldEqual result.toList
    }
  }
}
