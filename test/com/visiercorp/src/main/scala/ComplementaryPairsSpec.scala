package com.visiercorp.src.main.scala

import org.scalatest.{Matchers, FunSpec}

class ComplementaryPairsSpec extends FunSpec with Matchers {

  describe("k-complimentary pairs") {
    it("normal case") {
      ComplementaryPairs.solution(6, Vector(1, 8, -3, 0, 1, 3, -2, 4, 5)) shouldEqual 7
      ComplementaryPairs.solution(6, Vector(3, 3, 3)) shouldEqual 9
    }
    it("empty vector") {
      ComplementaryPairs.solution(100, Vector()) shouldEqual 0
    }
  }
}
