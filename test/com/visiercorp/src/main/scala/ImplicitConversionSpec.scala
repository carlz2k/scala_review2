package com.visiercorp.src.main.scala

import com.visiercorp.src.main.scala.ImplicitConversion.multiply
import org.scalatest.{FunSpec, Matchers}
import ImplicitConversion._

class ImplicitConversionSpec extends FunSpec with Matchers {

  //TODO these tests need more descriptive names
  describe("implicit conversion") {
    it("test") {
      val result = new Complex(1, 2).add(5)
      result.real shouldEqual 6
      result.imaginary shouldEqual 2
    }
  }

  describe("implicit parameter") {
    it("test") {
      implicit val v: Int = 5
      assert(multiply(10) == 50)
    }

    it("") {
      trait IService {
        def doSomething(i: Int): Int
      }

      implicit object SomeService extends IService {
        override def doSomething(i: Int): Int = i * 2 + 1
      }

      def callDoSomething(i: Int)(implicit someService: IService): Int = someService.doSomething(i)

      println(callDoSomething(10))
    }
  }
}
