package com.visiercorp.src.main.scala

import org.scalatest._

class ControlStructuresSpec extends FunSpec with Matchers {
  //what is the style for writing unit tests at visier?
  describe("get the product of unicode of a string using loop") {
    it("string Hello should produce 9415087488L") {
      ControlStructures.toUnicodeWithLoop("Hello") shouldEqual 9415087488L
    }

    it("empty string should produce 1") {
      ControlStructures.toUnicodeWithLoop("") shouldEqual 1
    }
  }

  describe("get the product of unicode of a string without using loop") {
    it("string Hello should produce 9415087488L") {
      ControlStructures.toUnicodeWithoutLoop("Hello") shouldEqual 9415087488L
    }

    it("empty string should produce 1") {
      ControlStructures.toUnicodeWithoutLoop("") shouldEqual 1
    }
  }

  describe("get the product of unicode of a string without using loop via a function") {
    it("string Hello should produce 9415087488L") {
      ControlStructures.toUnicodeWithoutLoopFunctionImplementation("Hello") shouldEqual 9415087488L
    }

    it("null or empty string should produce 0") {
      ControlStructures.toUnicodeWithoutLoopFunctionImplementation("") shouldEqual 1
    }
  }

  describe("get the product of unicode of a string via recursion") {
    it("string Hello should produce 9415087488L") {
      ControlStructures.toUnicodeTailRecursive("Hello") shouldEqual 9415087488L
    }

    it("null or empty string should produce 0") {
      ControlStructures.toUnicodeTailRecursive("") shouldEqual 1
    }
  }

  describe("power function") {
    it("return 1 when n = 0") {
      ControlStructures.power(3, 0) shouldEqual 1
    }

    it("when n is even") {
      ControlStructures.power(5, 4) shouldEqual 625
    }

    it("when n is odd") {
      ControlStructures.power(5, 5) shouldEqual 3125
    }

    it("when n is negative") {
      ControlStructures.power(5, -4) shouldEqual 1.0 / 625
      ControlStructures.power(5, -5) shouldEqual 1.0 / 3125
    }

    it("when x is 0 and n is positive should return 0") {
      ControlStructures.power(0, 5) shouldEqual 0
    }
  }
}