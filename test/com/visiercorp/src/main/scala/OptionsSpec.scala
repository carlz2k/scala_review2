package com.visiercorp.src.main.scala

import org.scalatest.{FunSpec, Matchers}

class OptionsSpec extends FunSpec with Matchers {
  describe("computes the sum of the non-<None> values in a <List[Option[Int]]>") {
    it("normal case") {
      OptionsExec.sumNonNones(List(Option(1), None, Some(2), Option(5), None)) shouldEqual 8
    }
  }

  def f(x: Double): Option[Double] = if (x >= 0) Some(math.sqrt(x)) else None

  def g(x: Double): Option[Double] = if (x != 1) Some(1 / (x - 1)) else None

  describe("compose functions") {
    it("test") {
      val h = OptionsExec.compose(f, g)

      h(5) shouldEqual Some(0.5)
    }

    it("test for none") {
      val h = OptionsExec.compose(f, g)

      h(1) shouldEqual None

      h(-4) shouldEqual None
    }
  }

  describe("compose functions with flatmap") {
    it("test") {
      val h = OptionsExec.composeWithFlatMap(f, g)

      h(5) shouldEqual Some(0.5)
    }

    it("test for none") {
      val h = OptionsExec.composeWithFlatMap(f, g)

      h(1) shouldEqual None

      h(-4) shouldEqual None
    }
  }
}
