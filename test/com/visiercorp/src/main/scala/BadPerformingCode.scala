package com.visiercorp.src.main.scala

import org.scalatest.FunSpec

//the strategy is to run profile and check the snapshot
//snapshot will provide you the functions that are running slow, and which object(s) clog most of the heap space
//then try to figure out a fix one by one

class BadPerformingCode extends FunSpec with PerfTiming {
  val NUM_TIMING_LOOPS = 5
  val ITERATIONS = 25000000

  //we could use 0 until ITERATIONS directly at the loop, instead of defining it and then converting it to a IndexedSeq which adds more overhead
  //val ITERATION_RANGE = 0 until ITERATIONS

  //val ITERATION_INT_SEQ: IndexedSeq[Int] = ITERATION_RANGE.toIndexedSeq

  //unused variable that causes the initialization of Main to be slow
  //val ITERATION_SEQ: IndexedSeq[Integer] = ITERATION_RANGE.map(i => Integer.valueOf(i))

  /**
    * @return default double value, possibly NaN
    */
  def defaultValue: Double = {
    var acc: Double = Double.NaN
    var i = 0
    while (i < 10) {
      acc += Math.random()
      i += 1
    }

    //not sure what is the point of doing this, but set to NaN and check NaN seem to be awefully expensive
    //will use another magic number
    //TODO I think changing the invalid number returned here is changing the spec too much
    if (acc != 0) Double.NaN else 0
  }

  class Measure {
    var value = 0L
    var observedRecord = 0L

    def update(rowValue: Double): Unit = {
      if (!rowValue.isNaN) {
        value += 1
      }
    }
  }

  describe("Calculate measures for our data set!") {
    var result = 0L
    var observedRecordsPerMeasure: Long = 0L
    //defaultValue contains a loop, should call it once instead of every iteration
    val defaultVal = defaultValue

    time(NUM_TIMING_LOOPS) {
      val measure = new Measure()
      //IndexedSeq is slow and memory heavy when trying to iterate through it
      //using List seems to serve the purpose just okay
      val measures = List(measure)
      //TODO since there's one Int per measure, could we get improvements by putting it an int inside the MeasureClass, and then
      // not need to index an array of ints to update it?
      //zip with index takes a long time and its value does not change, so we can pull it out of the loop and call it once
      //TODO you don't seem to use the index when you foreach the zippedMeasures. You can remove the zipWithIndex.
      val zippedMeasures = measures.zipWithIndex
      for (rowIdx <- 0 until ITERATIONS) {

        zippedMeasures.foreach { measureP =>
          measureP._1.update(
            //TODO these curly braces are unnecessary
            if (rowIdx % 2 == 0) defaultVal else rowIdx.toDouble
          )
          //is this what you looking for?
          //TODO yup, though since you increment observedRecord every time you call update, this line can be part of the update method now
          measureP._1.observedRecord += 1
        }
      }

      result = measure.value
      observedRecordsPerMeasure = measure.observedRecord
    }

    println(result, observedRecordsPerMeasure)
  }

}
