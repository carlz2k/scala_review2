package com.visiercorp.src.main.java

import org.scalatest.{FunSpec, Matchers}

class ConstructorsSpec extends FunSpec with Matchers {
  val licensePlate = "lic"
  val manufacturer = "man"
  val name = "name"
  val modelYear = 333
  val newLicensePlate = "new lic"

  describe("car implemented via java") {
    it("4 forms of constructors should work") {
      //TODO check that the default values were assigned correctly?
      val car1 = new com.visiercorp.src.main.java.Car("ab", "cd", 5, "dsf")
      val car2 = new com.visiercorp.src.main.java.Car("ab", "cd", 5)
      car2.licensePlate shouldEqual ""
      val car3 = new com.visiercorp.src.main.java.Car("ab", "cd", "dsf")
      car3.getModelYear shouldEqual -1
      val car4 = new com.visiercorp.src.main.java.Car("ab", "cd")
      car4.getModelYear shouldEqual -1
      car4.licensePlate shouldEqual ""
    }

    it("only licensePlate support read and write") {
      val car1 = new com.visiercorp.src.main.java.Car(manufacturer, name, modelYear, licensePlate)
      car1.licensePlate shouldEqual licensePlate
      car1.licensePlate = newLicensePlate
      car1.getManufacturer shouldEqual manufacturer
      car1.getName shouldEqual name
      car1.getModelYear shouldEqual modelYear
      car1.licensePlate shouldEqual newLicensePlate
    }
  }
}
